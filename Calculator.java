import java.util.Random;
public class Calculator{
	
	public static int sum(int a, int b){
		return a + b;
	}
	
	public static double squareRoot(int a){
		return Math.sqrt(a);
	}
	
	public static double randomNum(){
		Random rand = new Random();
		int x = rand.nextInt();
		return x;
	}
	
	public static double divide(int a, int b){
		if(a != 0 && b != 0){
			return a/b;
		}else{
			return 0;
		}
	}
}